import asyncio
import logging

import kombu
from celery import Celery

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# console = logging.StreamHandler()
# logger.addHandler(console)

class BrokenPipeException(Exception):
    pass

class Client:
    """
    Celery async client with autoreconnection support
    """

    def __init__(self, host, queue, user, password, polling_interval=0.5):
        self.host = host
        self.queue = queue
        self.user = user
        self.password = password
        self.polling_interval = polling_interval
        self.instance = self.create_instance()

    def create_instance(self):
        celery_app = Celery(self.queue)
        celery_app.conf.update(
            task_serializer='json',
            accept_content=['json'],
            result_serializer='json',
            timezone='Europe/Moscow',
            enable_utc=True,
            broker_url=f"amqp://{self.user}:{self.password}@{self.host}//",
            result_backend='rpc://',
            task_default_queue=self.queue,
            broker_pool_limit=1
        )
        logger.info(f"Connected to queue {self.queue}")
        return celery_app

    def get_instance(self):
        """
        Get celery-celery_tools agent instance
        :return:
        """
        if self.instance.finalized:
            self.create_instance()
        return self.instance

    async def call(self, task_name, args):
        """
        Async call task (by name) with args and poll for result
        Warning: Task can't change global state
        :param task_name: task id
        :param args: array with arguments
        :return: task execution result
        """
        logger.info("Calling with " + task_name)
        logger.info("Args: [" + (",".join(str(arg) for arg in args)) + "]")

        connection = None
        ok = False
        result = None
        while not ok:
            try:
                url = f'amqp://{self.user}:{self.password}@{self.host}//'
                connection = kombu.Connection(url)
                connection.connect()

                result = self.instance.send_task(task_name, args,
                                                 connection=connection,
                                                 queue=self.queue)
                logger.debug(f"Task {task_name} to queue {self.queue} is sent")
                count = 0
                while (not result.ready()):
                    logger.debug("Waiting for result " + str(count))
                    count += 1
                    if count > 240:
                        raise Exception("Something went wrong")
                    await asyncio.sleep(self.polling_interval)

                ok = True
            except Exception as e:
                if "Broken pipe" in str(e):
                    logger.info("Broken pipe detected, force reconnect")
                    break
                logger.info(f"Resending request ({e})")
                await asyncio.sleep(self.polling_interval)
                try:
                    if result:
                        try:
                            result.forget()
                        except Exception as skip:
                            logger.info(f"Skipping forget exception {skip}")
                            pass
                    connection.release()
                    connection.close()
                except Exception as e:
                    logger.debug(f"Skipping exception {e}")
                    pass

        if not ok:
            raise BrokenPipeException()

        if result.failed():
            logger.debug("Skip result")
            res = {}
        else:
            logger.debug("Got result")
            res = result.result
            logger.debug(f"Result is {res}")

        connection.release()
        connection.close()
        return res

    async def async_invoke(self, task_name, args, **kwargs):
        """
        Async invoke task (by name) with args (and no wait for result)
        :param task_name: task id
        :param args: array with arguments
        :return: task id
        """
        url = f'amqp://{self.user}:{self.password}@{self.host}//'
        logger.info("Request to URL " + url)
        while True:
            connection: kombu.Connection = None
            try:
                connection = kombu.Connection(url)
                connection.connect()
                a = self.instance.send_task(task_name, args,
                                            connection=connection,
                                            queue=self.queue, **kwargs)
                try:
                    connection.release()
                    connection.close()
                except Exception as ex:
                    logger.debug(f"Exception in request {ex}")
                    pass
                return a

            except Exception as error:
                logger.error("--- Connection error, retrying " + str(error))
                if connection:
                    try:
                        connection.release()
                        connection.close()
                    except Exception as cer:
                        logger.debug(f"Skip exception in connection {cer}")
                        pass
                await asyncio.sleep(self.polling_interval)

    def invoke(self, task_name, args, **kwargs):
        """
        Async invoke task (by name) with args (and no wait for result)
        :param task_name: task id
        :param args: array with arguments
        :return: task id
        """
        try:
            url = f'amqp://{self.user}:{self.password}@{self.host}//'
            logger.info("URL " + url)
            connection = kombu.Connection(url)
            connection.connect()
            a = self.instance.send_task(task_name, args, connection=connection,
                                        queue=self.queue,
                                        **kwargs)
            try:
                connection.release()
                connection.close()
            except Exception as e:
                logger.debug(f"Skip kombu error {e}")
                pass
            return a
        except Exception as e:
            logger.debug(f"Skip invoke error {e}")
            return None


class Expose():

    def __init__(self, host, queue, user, password, memory_limit=128 * 1024):
        self.host = host
        self.queue = queue
        self.user = user
        self.password = password
        self.memory_limit = memory_limit

    def get_application(self):
        user = self.user
        password = self.password
        host = self.host
        queue = self.queue

        app = Celery(
            queue,
            broker_url=f'amqp://{user}:{password}@{host}//',
            result_backend='rpc://')

        app.conf.update(
            task_serializer='json',
            accept_content=['json'],
            result_serializer='json',
            timezone='Europe/Moscow',
            enable_utc=True,
            result_backend="rpc://",
            task_default_queue=queue,
            worker_max_memory_per_child=self.memory_limit,
            worker_prefetch_multiplier=1
        )

        return app
