from setuptools import setup

from setuptools import find_packages

setup(name='celery_tools',
      version='0.1',
      description='Celery Tools',
      author='Dmitrii Zolotov',
      packages=find_packages(),
      author_email='dmitrii.zolotov@gmail.com',
      install_requires=[
            "celery==4.4.3",
            "kombu==4.6.9"
      ],
      setup_requires=[
            "flake8"
      ],
      tests_require=[
            "pytest",
      ]
      )
